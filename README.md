# vim Plug

[https://github.com/junegunn/vim-plug#neovim](vim-plug neovim)

# nvim

link ~/.config/nvim/ ~/dotfiles/nvim/

# install coc plugin
:CocInstall coc-css coc-elixir coc-emmet coc-eslint coc-go coc-json coc-snippets coc-tailwindcss coc-tsserver
