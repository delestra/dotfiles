set -sg escape-time 0
set -g prefix C-Space
set -g default-terminal "xterm"
set -g history-limit 30000
set -g history-file ~/.tmux_history
# source-file "${HOME}/.tmux/themes/default.tmux"

# -----------------------------------------------------------------------------
# Copy & Paste
# -----------------------------------------------------------------------------
set-option -g default-command "reattach-to-user-namespace -l zsh"
#set-option -g default-command "bash"

# -----------------------------------------------------------------------------
# Turn on window renumbering
# -----------------------------------------------------------------------------
set-option -g renumber-windows on

# -----------------------------------------------------------------------------
# Ensure terminal starts with its own colour scheme (helps Vim/Neovim themes to not break)
# -----------------------------------------------------------------------------
set-option -g default-terminal "xterm-256color"
set -g default-terminal "xterm-256color"
# set -g default-terminal "screen-256color"
# set -g default-terminal "screen-256color" # no S-Fx keys ;-(
set-option -ga terminal-overrides ",xterm-256color:Tc"
# and ensure the key-codes are xterm alike
set -g xterm-keys on

# -----------------------------------------------------------------------------
# Turn on Vim mode for movement
# Also allows easy upwards searching
# -----------------------------------------------------------------------------
setw -g mode-keys vi

# -----------------------------------------------------------------------------
# Vim visual selection and yank when in copy mode
# -----------------------------------------------------------------------------
bind Space copy-mode
bind C-Space copy-mode
# bind -t vi-copy v begin-selection
# bind -t vi-copy y copy-selection
# bind -t vi-copy Escape cancel
bind -Tcopy-mode-vi v send -X begin-selection
bind -Tcopy-mode-vi y send -X copy-selection
bind -Tcopy-mode-vi Escape send -X  cancel

# bind-key -t vi-copy v begin-selection
# bind-key -t vi-copy y copy-pipe "reattach-to-user-namespace pbcopy"

# unbind -t vi-copy Enter
# bind-key -t vi-copy Enter copy-pipe "reattach-to-user-namespace pbcopy"

# -----------------------------------------------------------------------------
# After we have something yanked back in Vim we can paste our yanked selection
# anywhere we are in insert mode with (prefix p). This works in the vim buffer,
# search and fuzzy finder buffer for example
# -----------------------------------------------------------------------------
bind p paste-buffer

# -----------------------------------------------------------------------------
# Easier for my brain to remember to split the pane with - and \
# which resemble a horizontal and vertical split respectively
# doesn't override default (prefix ") and (prefix &)
# -----------------------------------------------------------------------------
bind \\ split-window -h -c "#{pane_current_path}"
bind - split-window -c "#{pane_current_path}"

# -----------------------------------------------------------------------------
# bind c to open new window in the same path
# -----------------------------------------------------------------------------
bind c new-window -c "#{pane_current_path}"


# -----------------------------------------------------------------------------
# Put Ctrl-l back as Ctrl-u
# Here is how to do this good luck finding a key
# -----------------------------------------------------------------------------
#bind -n C-u send-keys 'C-l'

# -----------------------------------------------------------------------------
# Switch between tmux screen splits and Vim splits with the same keys!
# <C-h><C-j><C-k><C-l>
# -----------------------------------------------------------------------------
bind -n C-h run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)vim(diff)?$' && tmux send-keys C-h) || tmux select-pane -L"
bind -n C-j run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)vim(diff)?$' && tmux send-keys C-j) || tmux select-pane -D"
bind -n C-k run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)vim(diff)?$' && tmux send-keys C-k) || tmux select-pane -U"
bind -n C-l run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)vim(diff)?$' && tmux send-keys C-l) || tmux select-pane -R"
bind -n C-\\ run "(tmux display-message -p '#{pane_current_command}' | grep -iqE '(^|\/)vim(diff)?$' && tmux send-keys 'C-\\') || tmux select-pane -l"

# These are the rules you need to use True Color and Nova terminal ANSI colors
# If you want Tmux to use the Nova color scheme, ensure you have installed a Nova plugin for your terminal first
# set -g default-terminal "xterm-256color"
# set -ga terminal-overrides ",xterm-256color:Tc"
set -g pane-border-style 'fg=brightblack,bg=black'
set -g pane-active-border-style 'fg=brightwhite,bg=black'
set -g status-style 'fg=brightblack,bg=black'
set -g message-style 'fg=cyan, bg=black'
set -g clock-mode-colour cyan
