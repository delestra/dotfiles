# zmodload zsh/zprof

fpath+=$HOME/.zsh/pure
autoload -U promptinit; promptinit
# optionally define some options
PURE_CMD_MAX_EXEC_TIME=10
# change the path color
zstyle :prompt:pure:path color white
# change the color for both `prompt:success` and `prompt:error`
zstyle ':prompt:pure:prompt:*' color cyan
# turn on git stash status
zstyle :prompt:pure:git:stash show yes
prompt pure


# User configuration

# export MANPATH="/usr/local/man:$MANPATH"

# You may need to manually set your language environment
# export LANG=en_US.UTF-8

# Preferred editor for local and remote sessions
# if [[ -n $SSH_CONNECTION ]]; then
#   export EDITOR='vim'
# else
#   export EDITOR='mvim'
# fi

# Compilation flags
# export ARCHFLAGS="-arch x86_64"

# ssh
# export SSH_KEY_PATH="~/.ssh/rsa_id"

# Set personal aliases, overriding those provided by oh-my-zsh libs,
# plugins, and themes. Aliases can be placed here, though oh-my-zsh
# users are encouraged to define aliases within the ZSH_CUSTOM folder.
# For a full list of active aliases, run `alias`.
#
# Example aliases
# alias zshconfig="mate ~/.zshrc"
# alias ohmyzsh="mate ~/.oh-my-zsh"
alias vim="/usr/local/bin/nvim"
alias loadnvm=". $(brew --prefix nvm)/nvm.sh"
alias ll='ls -l'
alias la='ls -A'

export PATH="/usr/local/bin:$PATH"

# # export node 8 from brew
# export PATH="/usr/local/opt/node@8/bin:$PATH"

export PATH="~/workspace/github/lodelestra/scripts:$PATH"


export PATH="/usr/local/opt/avr-gcc@7/bin:$PATH"

# Go development
export GOPATH="${HOME}/workspace/go"
export GOROOT="$(brew --prefix golang)/libexec"
export PATH="$PATH:${GOPATH}/bin:${GOROOT}/bin"

# eval "$(pyenv init -)"
# export PATH="/usr/local/opt/openssl/bin:$PATH"

# Symfony php framework
export PATH="$HOME/.symfony/bin:$PATH"

# Python
export PATH="$HOME/Library/Python/3.5/bin:$PATH"

# zprof
